import { Outlet } from "react-router-dom";
import Navbar from "../components/Common/Navbar";
import styled from "styled-components";

const StyledAppLayout = styled.div``;
const Main = styled.main``;

function AppLayoutPage() {
  return (
    <StyledAppLayout>
      <Navbar />
      <Main>
        <Outlet />
      </Main>
    </StyledAppLayout>
  );
}

export default AppLayoutPage;
