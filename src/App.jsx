import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import PageNotFound from './pages/NotFoundPage';
// import ProtectedRoute from "./components/ProtectedRoute";
import LoginPage from "./pages/Login";
import AppLayoutPage from "./pages/AppLayout";
import ChatPage from "./pages/Chat";
import UsersPage from "./pages/Users";

import { AuthProvider } from './context/AuthContext';


function App() {
  return (
    <div className="App">
      <AuthProvider>
        <BrowserRouter>
          <Routes>
            <Route element={<AppLayoutPage />} >
              <Route index element={<Navigate replace to="match" />} />
              <Route path="users" element={<UsersPage />} />
              <Route path="chat" element={<ChatPage />} />
              <Route path="match" element={<MatchPage />} />
            </Route>
            <Route path="login" element={<LoginPage />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </BrowserRouter>
      </AuthProvider>

    </div>
  );
}

function MatchPage() {
  return <div>
    <h1> Este es un h1 Dashboard</h1>
  </div>
}

export default App;
